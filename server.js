var express = require('express');
var app = express();
var imaps = require('imap-simple');
var fs = require('fs');
var utf8 = require('utf8');
var quotablePrintable = require('quoted-printable');

if (!fs.existsSync('EmailServer.json')) {
    fs.writeFileSync('EmailServer.json', JSON.stringify({
        Addresses: []
    }, null, 2));
}

var EmailServer = JSON.parse(fs.readFileSync('EmailServer.json', "utf8"));

var config = {
    imap: {
        user: 'your@gmail.com',
        password: 'yourpassword',
        host: 'imap.gmail.com',
        port: 993,
        tls: true,
        tlsOptions: { rejectUnauthorized: false }
    }
};

var Domains = [
    "discord-doesnt.work"
] //your domains

imaps.connect(config).then(function (connection) {
    connection.openBox('INBOX').then(function () {
        var searchCriteria = [
            'UNSEEN'
        ];
        var fetchOptions = {
            bodies: ['HEADER', 'TEXT'],
            markSeen: true
        };
        setInterval(() => {
            connection.search(searchCriteria, fetchOptions).then(function (results) {
                for(var result of results) {
                    var body = utf8.decode(quotablePrintable.decode(result.parts[0].body));
                    body = body.replace(body.split('<')[0], '');
                    body = body.replace(body.split('>')[body.lastIndexOf('>') + 1], '');
                    var subject = result.parts[1].body.subject[0];
                    var to = result.parts[1].body.to[0];
                    var from = result.parts[1].body.from[0];
                    var date = result.parts[1].body.date[0];
                    if (Domains.filter(x => x.toLowerCase().includes(to.split('@')[1])).length > 0) {
                        var address = EmailServer.Addresses.filter(x => x.Address == to)[0];
                        if (address) { 
                            address.Emails.push({
                                Headers: {
                                    To: to,
                                    From: from,
                                    Subject: subject,
                                    Date: date
                                },
                                Body: body
                            });
                            fs.writeFileSync('EmailServer.json', JSON.stringify(EmailServer, null, 2));
                        } else {
                            EmailServer.Addresses.push({
                                Address: to,
                                Emails: [
                                    {
                                        Headers: {
                                            To: to,
                                            From: from,
                                            Subject: subject,
                                            Date: date
                                        },
                                        Body: body
                                    }
                                ]
                            });
                            fs.writeFileSync('EmailServer.json', JSON.stringify(EmailServer, null, 2));
                        }
                    }
                }
            });
        }, 3000);
    });
});

app.get('/api/inbox/:email', (req, res) => {
    try {
        if (!req.params.email) {
            return res.status(400).json({message: "Expected email but received none."});
        }
    
        var inbox = EmailServer.Addresses.filter(x => x.Address == req.params.email)[0];
        if (!inbox) {
            return res.status(400).json({message: "There are no emails for this address yet."});
        }
    
        res.json(inbox.Emails);
        EmailServer.Addresses.splice(EmailServer.Addresses.indexOf(inbox), 1);
        fs.writeFileSync('EmailServer.json', JSON.stringify(EmailServer, null, 2));
    }
    catch(err) {
        console.log(err);
        return res.status(500).json({message: "Internal Server Error."});
    }
});

app.get('/api/inbox/:email/latest', (req, res) => {
    try {
        if (!req.params.email) {
            return res.status(400).json({message: "Expected email but received none."});
        }
    
        var inbox = EmailServer.Addresses.filter(x => x.Address == req.params.email)[0];
        if (!inbox) {
            return res.status(400).json({message: "There are no emails for this address yet."});
        }
    
        res.json(inbox.Emails[inbox.Emails.length]);
        EmailServer.Addresses.splice(EmailServer.Addresses.indexOf(inbox), 1);
        fs.writeFileSync('EmailServer.json', JSON.stringify(EmailServer, null, 2));
    }
    catch(err) {
        console.log(err);
        return res.status(500).json({message: "Internal Server Error."});
    }
});

app.listen(9999);
